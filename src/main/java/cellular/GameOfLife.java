package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;
	int rows;
	int columns;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		int i;
		for(i=0; i<rows;i++){
		}
		return i;
	}

	@Override
	public int numberOfColumns() {
		int i;
		for(i=0; i<columns; i++){
		}
		return i;
	}

	@Override
	public CellState getCellState(int row, int col) {
		if (row > rows){
            throw new IndexOutOfBoundsException();
        }
        if (row < 0){
            throw new IndexOutOfBoundsException();
        }
        if (col > columns){
            throw new IndexOutOfBoundsException();
        }
        if (col < 0){
            throw new IndexOutOfBoundsException();
        }
        else{
			CellState val = currentGeneration.get(row, col);
			return val;
		}
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for(int i=0; i<numberOfRows(); i++){
			for(int b=0; b<numberOfColumns(); b++){
				nextGeneration.set(i, b, getNextCell(i, b));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		if(getCellState(row, col) == CellState.ALIVE && (countNeighbors(row, col, CellState.ALIVE)<2)){
			return CellState.DEAD;
		}
		else if(getCellState(row, col) == CellState.ALIVE && (countNeighbors(row, col, CellState.ALIVE) == 2)){
			return CellState.ALIVE;
		}
		else if(getCellState(row, col) == CellState.ALIVE && (countNeighbors(row, col, CellState.ALIVE)>3)){
			return CellState.DEAD;
		}
		else if(getCellState(row, col) == CellState.ALIVE && (countNeighbors(row, col, CellState.ALIVE) == 3)){
			return CellState.ALIVE;
		}
		else if(getCellState(row, col) == CellState.DEAD && (countNeighbors(row, col, CellState.ALIVE) == 3)){
			return CellState.ALIVE;
		}
		else{
			return CellState.DEAD;
		}
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int statecount = 0;

		for(int i : new int[]{-1, 0, 1}){
			for(int b : new int[]{-1, 0, 1}){
				if(i==0 && b==0){
					continue;
				}
				CellState currentState = currentGeneration.get(Math.floorMod(row+i, numberOfRows()), Math.floorMod(col+b, numberOfColumns()));
				if(currentState.equals(state)){
					statecount++;
				}
			}
		}
		return statecount;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
