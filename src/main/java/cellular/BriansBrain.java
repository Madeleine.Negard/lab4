package cellular;

import datastructure.IGrid;
import datastructure.CellGrid;

public class BriansBrain implements CellAutomaton{

    int rows;
    int columns;
    IGrid currentGeneration;
    public BriansBrain(int i, int j) {
        this.rows = i;
        this.columns = j;
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
    }
   

    
    @Override
    public CellState getCellState(int row, int column) {
        if (row > rows){
            throw new IndexOutOfBoundsException();
        }
        if (row < 0){
            throw new IndexOutOfBoundsException();
        }
        if (column > columns){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0){
            throw new IndexOutOfBoundsException();
        }
        else{
			CellState val = currentGeneration.get(row, column);
			return val;
		}
    }

    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for(int i=0; i<numberOfRows(); i++){
			for(int b=0; b<numberOfColumns(); b++){
				nextGeneration.set(i, b, getNextCell(i, b));
			}
		}
		currentGeneration = nextGeneration;
	}
        

    @Override
    public CellState getNextCell(int row, int col) {
        if(getCellState(row, col) == CellState.ALIVE){
            return CellState.DYING;
        }
        else if(getCellState(row, col) == CellState.DYING){
            return CellState.DEAD;
        }
        else if(getCellState(row, col) == CellState.DEAD && countNeighbors(row, col, CellState.ALIVE) == 2){
            return CellState.ALIVE;
        }
		else{
			return CellState.DEAD;
		}
    }

    @Override
    public int numberOfRows() {
        int i;
		for(i=0; i<rows;i++){
		}
		return i;
    }

    @Override
    public int numberOfColumns() {
        int i;
		for(i=0; i<columns; i++){
		}
		return i;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    

    private int countNeighbors(int row, int col, CellState state) {
        int statecount = 0;

        for(int i : new int[]{-1, 0, 1}){
            for(int b : new int[]{-1, 0, 1}){
                if(i==0 && b==0){
                    continue;
                }
                CellState currentState = currentGeneration.get(Math.floorMod(row+i, numberOfRows()), Math.floorMod(col+b, numberOfColumns()));
                if(currentState.equals(state)){
                statecount++;
                }
            }
        }
        return statecount;
    }
}

