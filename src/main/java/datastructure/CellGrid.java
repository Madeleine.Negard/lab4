package datastructure;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import cellular.CellState;

public class CellGrid implements IGrid {
    public CellState[][] grid;
    int rows;
    int columns;
    CellState initialState;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];
        for (int row=0; row<rows; row++){
            for (int col=0; col<columns; col++){
                grid[row][col] = initialState; 
            }
        }
	}

    @Override
    public int numRows() {
        int i;
        for (i = 0; i<rows;i++){
        }
        return i;
    }

    @Override
    public int numColumns() {
        int i;
        for (i=0; i<columns; i++){
        }
        return i;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row > rows){
            throw new IndexOutOfBoundsException();
        }
        if (row < 0){
            throw new IndexOutOfBoundsException();
        }
        if (column > columns){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0){
            throw new IndexOutOfBoundsException();
        }
        else{
            grid[row][column] = element;
        }

    }

    @Override
    public CellState get(int row, int column) {
        if (row > rows){
            throw new IndexOutOfBoundsException();
        }
        if (row < 0){
            throw new IndexOutOfBoundsException();
        }
        if (column > columns){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0){
            throw new IndexOutOfBoundsException();
        }
        else{
            return grid[row][column];
        }
    }

    @Override
    public IGrid copy() {
        IGrid newgrid = new CellGrid(numRows(), numColumns(), initialState);
        for (int i=0; numRows()>i; i++){
            for (int b=0; numColumns()>b; b++){
                CellState val = get(i,b);
                newgrid.set(i, b, val);
            }
        }
        return newgrid;
    }
    
}
